# MyCar

#### 介绍
小车程序代码

#### 软件架构
软件架构说明
 V1是用Java写的一个简单的gpio控制的代码，此处使用了Java的pi4j库，如果有需要的小伙伴儿，后续会上传。
 V2是用Python写的虽然可以控制小车的云台上下左右，小车方向和前进后退，但是树莓派3b+，4B的pwm信号不稳定。
 V3是用Python写的，在V2的基础上，通过python的pigpio库，优化了舵机角度的精准控制和pwm信号不稳定抖动问题。
#### 安装教程

1. 安装pigpio
#### sudo apt-get install pigpio python-pigpio python3-pigpio
2. 启动后台进程
#### 启动  sudo pigpiod
     停止 sudo killall pigpiod
  开机自启
  sudo systemctl enable pigpiod
   
3. 运行mycarV3.py
#### sudo python3 mycarV3.py

#### 使用说明

1. 此处控制端 用到了 爆改车间主任的app，表示感谢,他这app使用udp协议传输数据的。
![输入图片说明](%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20220726164933.jpg)
2. 参数配置
![输入图片说明](%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20220726164547.jpg)
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

import java.util.Scanner;
ww
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;

public class MyCarV1{
	public static void main(String[] args) {
		System.out.println("=====小车开程序始运行=====");

		final GpioController gpio = GpioFactory.getInstance();
		final GpioPinDigitalOutput pin1 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_01,"left",PinState.LOW);
		final GpioPinDigitalOutput pin2 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_04,"right",PinState.LOW);
		final GpioPinDigitalOutput pin3 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_05,"left",PinState.LOW);
		final GpioPinDigitalOutput pin4 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_06,"right",PinState.LOW);
		pin1.setShutdownOptions(false, PinState.LOW); // A使能前进
		pin2.setShutdownOptions(false, PinState.LOW); // B使能前进
		pin3.setShutdownOptions(false, PinState.LOW); // A使能后退
		pin4.setShutdownOptions(false, PinState.LOW); // B使能后退
		
		System.out.println("=====小车启动====");
		System.out.println("==============");
		System.out.println("======开始=====");
		
		Scanner input = new Scanner(System.in);
		for (int i = 0; i < 100; i++) {
			System.out.println("======前进=====按1");
			int A = input.nextInt();
			if (A == 1) {
				System.out.println("前进中");
				pin1.toggle();
				pin2.toggle();
			}
			System.out.println("======停止=====按2");
			    A = input.nextInt();
			if (A == 2) {
				System.out.println("已停止");
				pin1.toggle();
				pin2.toggle();
			}
		}

		gpio.shutdown();

		System.out.println("====程序执行完毕====");
	}
}

import socket
import RPi.GPIO as GPIO
import time
import signal
import atexit
atexit.register(GPIO.cleanup) 

GPIO.setmode(GPIO.BCM)
#云台控制
GPIO.setup(13,GPIO.OUT,initial=False)
yt0 = GPIO.PWM(13,50)
yt0.start(0)
GPIO.setup(19,GPIO.OUT,initial=False)
yt1 = GPIO.PWM(19,50)
yt1.start(0)
#方向控制
GPIO.setup(18,GPIO.OUT,initial=False)
se = GPIO.PWM(18,50)
se.start(0)

GPIO.setup(16, GPIO.OUT, initial=False)
b = GPIO.PWM(16,50) #50H
b.start(0)
GPIO.setup(12, GPIO.OUT, initial=False)
m = GPIO.PWM(12,50) #50H
m.start(0)

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.bind(("192.168.1.7", 13480))  # 绑定服务器的ip和端口
while True:
    data = s.recv(1024).decode('utf-8')  # 一次接收1024字节
    if data=="sx,":
       continue  
    data1=data.split(",")
    direction_lx=data1[0]#获取左边x的值
    lx = direction_lx.split("lx")[1]
    lxs=float(lx)
    sjd= float(2.5 + 10 * (lxs) / 180)
    dj= round(sjd, 2)
    print(dj)
    se.ChangeDutyCycle(dj)
 
    direction_ly = data1[1]  # 获取左边y的值
    ly =  direction_ly.split("ly")[1]
    dc= float(ly)
    dcr= round(dc, 2)
    if dcr >50 :
       p2=(dcr-50)*2
       if p2<10:
          m.ChangeDutyCycle(0) 
       else:    
          m.ChangeDutyCycle(p2)
    else:
       p1=(50-dcr)*2
       if p1<10:
          b.ChangeDutyCycle(0) 
       else:    
          b.ChangeDutyCycle(p1)
    #上下旋转
    direction_ry = data1[3]  # 获取右边y的值
    ry = direction_ry.split("ry")[1]
    rys=float(ry)
    ryskj= float(2.5 + 10 * (rys) / 180)
    ryskjz= round(ryskj, 2)
    if ryskjz==5.83:
      yt0.ChangeDutyCycle(0)
    else:
      yt0.ChangeDutyCycle(ryskjz)
    #左右旋转
    direction_rx = data1[2]  # 获取右边x的值
    rx = direction_rx.split("rx")[1]
    rxs=float(rx)
    rxskj= float(2.5 + 10 * (rxs) / 180)
    rxskjz= round(rxskj, 2)
    if rxskjz==5.83:
      yt1.ChangeDutyCycle(0)
    else:
      yt1.ChangeDutyCycle(rxskjz)

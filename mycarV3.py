import pigpio
import socket
import RPi.GPIO as GPIO


#前进控制使能
GPIO.setmode(GPIO.BCM)
GPIO.setup(16, GPIO.OUT, initial=False)
b = GPIO.PWM(16,50) #50H
b.start(0)

#后退控制使能
GPIO.setup(12, GPIO.OUT, initial=False)
m = GPIO.PWM(12,50) #50H
m.start(0)

yt0 = pigpio.pi()#底座左右
yt0.set_PWM_frequency(19, 50)#设定19号引脚产生的pwm波形的频率为50Hz
yt0.set_PWM_range(19, 1000)#指定要把19号引脚上的一个pwm周期分成多少份，这里是分成2000份，这个数据的范围是25-40000        
yt1 = pigpio.pi()#摄像头上下
yt1.set_PWM_frequency(13, 50)
yt1.set_PWM_range(13, 2000)
zx = pigpio.pi()#转向控制
zx.set_PWM_frequency(18, 50)
zx.set_PWM_range(18, 1000)
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.bind(("127.0.0.1", 13480))  # 绑定服务器的ip和端口  写自己路由器的内网IP如192.168.1.6
while True:
    data = s.recv(1024).decode('utf-8')  # 一次接收1024字节
    if data=="sx,":
       continue
    data1=data.split(",")
    direction_lx=data1[0]#获取左边x的值
    lx = direction_lx.split("lx")[1]
    lxs=float(lx)
    #print(lxs)
    zx.set_PWM_dutycycle(18,lxs+25)
    
    direction_ly = data1[1]  # 获取左边y的值
    ly =  direction_ly.split("ly")[1]
    dc= float(ly)
    dcr= round(dc, 2)
    #取y正半轴
    if dcr >50 :
       p2=(dcr-50)*2
       if p2<10:
          m.ChangeDutyCycle(0) 
       else:    
          m.ChangeDutyCycle(p2)
     #取y负半轴      
    else:
       p1=(50-dcr)*2
       if p1<10:
          b.ChangeDutyCycle(0) 
       else:    
          b.ChangeDutyCycle(p1)
          
    data1=data.split(",")
    direction_rx = data1[2]  # 获取右边x的值
    rx = direction_rx.split("rx")[1]
    rxs=float(rx)
    #print(rxs)
    yt0.set_PWM_dutycycle(19,rxs+30) #指定pwm波形的占空比，这里的占空比为150/2000,2000是上一个函数设定的
    direction_ry = data1[3]  # 获取右边y的值
    ry = direction_ry.split("ry")[1]
    rys=float(ry)
    yt1.set_PWM_dutycycle(13,rys+25)
    